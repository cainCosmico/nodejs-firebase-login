const { Router } = require('express');
const router = Router();
const admin = require('firebase-admin');

var serviceAccount = require("../../service-account.json");

admin.initializeApp({
   credential: admin.credential.cert(serviceAccount),
   databaseURL: 'https://node-firebase-example-5e7c1.firebaseio.com/',
});

const db = admin.database();

router.get('/', (req,res) => {

   console.log('Index works!');
   // res.send('Received');
   db.ref('contacts').once('value', (snpshot) => {
      const data = snpshot.val();
      res.render('index', { contacts: data });      
   });
   
});

router.post('/new-contact', (req,res) => {
   console.log(req.body);
   const newContact = {
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      email: req.body.email,
      phone: req.body.phone,
   };
   console.log(newContact);
   db.ref('contacts').push( newContact );
   res.redirect('/');
});

router.get('/delete-contact/:id', (req,res) => {
   console.log('Delete: ',req.params.id);
   db.ref('contacts/' + req.params.id).remove();
   res.redirect('/');
});

module.exports = router;
